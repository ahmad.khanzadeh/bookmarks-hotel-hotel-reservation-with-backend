import { useState } from "react";

export default function useGeoLocation(){
    const [isLoading, setIsLoading]= useState(false);
    const [position, setPosition]= useState({});
    const [error, setError]= useState(null);

    
    function getPosition(){
        if(!navigator.geolocation)
            return setError("sorry, your browser does not support the geolocation. ");
        setIsLoading(true);
        //get the user location from browser.
        //2 scenarios: user give=>first arror function. user reject => second arrow function
        navigator.geolocation.getCurrentPosition(
            (pos)=>{
                setPosition({
                    lat: pos.coords.latitude,
                    lng: pos.coords.longitude,
                });
                setIsLoading(false);
            },
            (error)=>{
                setError(error.message);
                setIsLoading(false)
            }
        );
    }

    return{isLoading, error, position,getPosition};
}