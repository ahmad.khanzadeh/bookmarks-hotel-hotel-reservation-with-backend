
import "./App.css";
import Header from "./components/Header/Header";
// calender style import 
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import { Toaster } from "react-hot-toast"
import LocationList from "./components/LocationList/LocationList";
import { Route, Routes } from "react-router-dom";
import AppLayout from "./components/AppLayout/AppLayout";
import Hotels from "./components/Hotels/Hotels";
import HotelsProvider from "./components/context/HotelsProvider";
import SingleHotel from "./components/singleHotel/SingleHotel";
import BookmarkLayout from "./components/bookmarkLayout/BookmarkLayout";
import BookmarkListProvider from "./components/context/BookmarkListContext";
import Bookmark from "./components/Bookmark/Bookmark";
import SingleBookmark from "./components/SingleBookmark/SingleBookmark";
import AddNewBookmark from "./components/AddNewBookmark/AddNewBookmark";
import AuthContextProvider from "./components/context/AuthProvider";
import Login from "./components/Login/Login";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";

function App() {
  return <div> 
  <AuthContextProvider>
  <BookmarkListProvider>
  <HotelsProvider>
  <Toaster />
    <Header />
    <Routes>
      <Route path="/" element={<LocationList />} />
      <Route path="/hotels" element={<AppLayout />}>
        <Route index element={<Hotels></Hotels>} />
        <Route path=":id" element={<SingleHotel />} />
      </Route>
     <Route path="/bookmark" element={<ProtectedRoute><BookmarkLayout /></ProtectedRoute>} >
        <Route index element={<Bookmark />} />
        <Route path=":id" element={<SingleBookmark />} />
        <Route path="add" element={<AddNewBookmark />} />
     </Route>
     <Route path="/login" element={<Login />} ></Route>
    </Routes>
  </HotelsProvider>
  </BookmarkListProvider>
  </AuthContextProvider>

  </div>
}

export default App;

