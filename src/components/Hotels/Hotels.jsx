import React from 'react'
import { useSearchParams , Link} from 'react-router-dom'
import Loader from '../Loader/Loader';
import useFetch from '../../hooks/useFetch';
import { useHotels } from '../context/HotelsProvider';

const Hotels = () => {
   /* const [searchParams, setSearchParams]=useSearchParams();
    //to extract informations that user already asked in header section
    // remember: params will return a string, turn it to object by JSON.parse()
    const destination=searchParams.get("destination");
    const room=JSON.parse(searchParams.get("options"))?.room;

    // fetch data from localhost-pass condition which user selected to the backend
    const {isLoading, data}=useFetch(
        "http://localhost:5000/hotels",
        `name_like=${destination || ""}&accommodates_gte=${room || 1}`);
        // `host_location_like=${destination || ""}&name_like=${destination || ""}&accommodates_gte=${room || 1}`
        // _like => is a kind of operator to look for the same results in table
        */
    //    the upper part was moved to the context > HotelsProdiver.jsx
        const {isLoading, hotels, currentHotel} = useHotels();

        if(isLoading) <Loader /> ;
        return <div className="searchList">
            <h2>Search results ({hotels.length})</h2>
            {
                hotels.map(item =>{
                    return <Link key={item.id} to={`/hotels/${item.id}?lat=${item.latitude}&lng=${item.longitude}`}>
                                {/* add a style just for former selected hotels */}
                                <div className={`searchItem ${item.id === currentHotel?.id ? "current-hotel": ""}`}>
                                    <img src={item.picture_url.url} alt={item.name}/>
                                    <div className="searchItemDesc">
                                        <p className="location">{item.smart_location}</p>
                                        <p className="name">{item.name}</p>
                                        <p className="price">€ &nbsp; {item.price} &nbsp; <span>night</span></p>
                                    </div>
                                </div>
                            </Link>
                })
            }
        </div>;
}

export default Hotels