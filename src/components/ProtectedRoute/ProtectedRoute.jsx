import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthProvider';


const ProtectedRoute = ({children}) => {
    const { isAuthenticated } = useAuth();
    const navigate= useNavigate();

    //in start and in vase Authentication is changed, navigate to main menu
    useEffect(()=>{
        if(!isAuthenticated) { navigate("/");}
    },[isAuthenticated,navigate]);

  return isAuthenticated ? children : null;
}

export default ProtectedRoute