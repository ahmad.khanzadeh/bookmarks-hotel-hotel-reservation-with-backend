import React from 'react'
import { useBookmark } from '../context/BookmarkListContext'
import Loader from '../Loader/Loader'
import ReactCountryFlag from 'react-country-flag'
import { Link } from 'react-router-dom'
import { HiTrash } from "react-icons/hi";

const bookmark = () => {
    const {isLoading,bookmarks, currentBookmark, deleteBookmark}=useBookmark();

    // deleting function will send data to backend and get confirmation --> async
    const handleDelete= async (e,id) => {
      // since click on the item will redirect to a single bookmark page, we stop the procedure
      e.preventDefault();
      await deleteBookmark(id);
    }

    if(isLoading) return <Loader />
    if(!bookmarks.length) return <p>There are no bookmarked location in the moment...</p>
  return (
    <div>
        <h2>BookmarkList</h2>
        <div className="bookmarkList">
            {bookmarks.map((item) => {
                return(
                    <Link key={item.id} to={`${item.id}?lat=${item.latitude}&lng=${item.longitude}`}>
                        <div className={`bookmarkItem ${item.id=== currentBookmark?.id ? "current-bookmark" : ""}`}> 
                          <div>
                             <ReactCountryFlag svg countryCode={item.countryCode} />
                               &nbsp; <strong>{item.cityName}</strong> &nbsp; <span>{item.country}</span>
                          </div>
                          <button onClick={(e) => handleDelete(e, item.id)}>
                            <HiTrash  className="trash" />
                          </button>
                        </div>
                    </Link>
                ) 
            })}
        </div>
    </div>
  )
}

export default bookmark