import React from 'react'
import { useParams } from 'react-router-dom'
import useFetch from '../../hooks/useFetch'
import Loader from '../Loader/Loader'
import { useHotels } from '../context/HotelsProvider'
import { useEffect } from 'react'

const SingleHotel = () => {
    // extract the user search data from the address 
    const {id}= useParams();
    // fetch the hotel data - custom hook
    // const { isLoading, data} = useFetch(`http://localhost:5000/hotels/${id}`);
    //to fetch data from context - display last visited hotels
    const {getHotel, isLoadingCurrHotel, currentHotel} = useHotels();
  
    useEffect(()=>{
        getHotel(id);
    },[id])

    // if the site is in loading condition, show loader
    // if(isLoading) return <Loader />;
    if(isLoadingCurrHotel || !currentHotel) return <Loader />;

    
    return (      
    <div className="room">
        <div className="roomDetail">
            <h2>{currentHotel.name}</h2>
            <div>
                {currentHotel.number_of_reviews} reviews &bull; {currentHotel.smart_location}
            </div>
            <img src={currentHotel.xl_picture_url} alt={currentHotel.name} />
        </div>
    </div>
  )
}

export default SingleHotel