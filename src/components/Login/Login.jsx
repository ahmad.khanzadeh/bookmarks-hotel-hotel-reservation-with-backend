import React , {useEffect, useState} from 'react'
import { useAuth } from '../context/AuthProvider';
import { useNavigate } from 'react-router-dom';


const Login = () => {
    const [email,setEmail]= useState("samyar@gmail.com");
    const [password, setPassword]= useState("1234");
    // from the last function in AuthProvider.jsx 
    const {user,login, isAuthenticated}=useAuth();
    // navigate user after login in 
    const navigate=useNavigate();

    const handleSubmit=(e)=>{
        e.preventDefault();
        //if there were email and password, set the login condition:
        if(email && password){ login(email,password);}
    }
    // console.log(user); ok by pressing the submit button, user is made

    useEffect(()=>{
            // the replace: true will prevent the reloading( data will be in the )
            //also, if the user press the back button, he will not get to the login page again
            if(isAuthenticated) {navigate("/",{replace: true});}
    },[isAuthenticated,navigate]);

  return (
    <div className="loginContainer">
        <h2>Login</h2>
        <h6>username: samyar@gmail.com</h6>
        <h6>password: 1234</h6>
        <form action="" className="form" onSubmit={handleSubmit}>
            <div className="formControl">
                <label htmlFor="email">Email</label>
                <input value={email} onChange={(e) => setEmail(e.target.value)} type="text" name="email" id="email"/>
            </div>
            <div className="formControl">
                <label htmlFor="password">password</label>
                <input value={password} onChange={(e) => setPassword(e.target.value)} type="password" name="password" id="password"/>
            </div>
            <div className="buttons">
                    <button className="btn btn--primary">Login</button>
            </div>
        </form>
    </div>
  );
}

export default Login