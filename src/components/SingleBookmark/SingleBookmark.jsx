import React from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { useBookmark } from '../context/BookmarkListContext';
import Loader from '../Loader/Loader';
import { useEffect } from 'react';
import ReactCountryFlag from 'react-country-flag';

const SingleBookmark = () => {
    // just like the singlehotel, all bookmarks will called here
    // like Bookmar.jsx, the getBookmark will be called here from the custom hook
        const {id}= useParams();
        const {isLoading,getBookmark, currentBookmark}= useBookmark();
        // for the go back button: 
        const navigete=useNavigate();

    useEffect(()=>{getBookmark(id)},[id]);

    const handleBack = (e) =>{
        e.preventDefault();
        navigete(-1);
    }

    // if the async function in BookmarkListContext is not loaded( first line after tha async function)
    if(isLoading || !currentBookmark) return <Loader />
  return (
    <div>
    <button onClick={handleBack} className="btn btn--back">&larr; </button>
        <h2>{currentBookmark.cityName}</h2>
        <p>{currentBookmark.cityName} - {currentBookmark.country}</p>
        <ReactCountryFlag svg countryCode={currentBookmark.countryCode} />
    </div>
  )
}

export default SingleBookmark