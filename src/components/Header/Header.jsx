import React from 'react'
import { MdAddLocationAlt } from "react-icons/md";
import { FaNeuter } from "react-icons/fa6";
import { HiCalendar } from "react-icons/hi";
import { useState } from 'react';
import { useRef } from 'react';
// import  useOutsideClick  from '../../hooks/useOutsideClick';
import useOutsideClick from '../../hooks/useOutsideClick';
import { DateRange } from 'react-date-range';
import { format } from 'date-fns';
import { NavLink, createSearchParams, useNavigate, useSearchParams } from 'react-router-dom';
import { useAuth } from '../context/AuthProvider';


const Header = () => {
    // what did user search and pass it to the route:
    const [searchParams, setSearchParams]=useSearchParams();
    //user destination
    const [destination,setDestination] = useState(searchParams.get("destination") || "");
    // open or close the target
    const [openOptions, setOpenOptions]= useState(false);
    // number of room members in header
    const [options, setOptions]=useState({ adult:1, children:0,room:1,});
    // to get the date of start and end-since date only accept range, we have to make it as a range here-key is used in onChange of the <DateRange /> 
    const [date, setDate]=useState([{startDate:new Date(), endDate:new Date(), key: "selection",},])
    //view and hide the calender
    const [openDate,setOpenDate]=useState(false);
    // for search navigation
    const navigate=useNavigate();
    


    // - or + in buttons
    const handleOptions=(name, operation) =>{
        setOptions((prev)=>{
            return{
                ...prev,
                [name]: operation==='inc' ? options[name]+1 : options[name]-1
            }
        })
    }

    // a function to navigate the search menu : 
    const handleSearch=()=>{
        // created by react router dom -extract data from the other header sections
        // and turn it to a string for address bar 
       const encodedParams= createSearchParams({
            date:JSON.stringify(date),
            destination,
            options:JSON.stringify(options)
        });
       /* // Action =>  add the upper data to the params 
        setSearchParams(encodedParams);
        */
        // or we could pass data directly to the navigate({}) but it is not perfessional
        navigate({
            pathname: "/hotels",
             search: encodedParams.toString(),
            });
        // navigate("/hotels")
    };


    // a function to navigate user to a specific page:
    const handleFormerBookmarks=(e)=>{
        navigate({
            pathname:"/bookmark"
        })
    }

    const handleLogin=(e)=>{
        navigate({
            pathname:"/login"
        })
    }

    return (
    <div className="header">
        <div className="headerSearch">
            <div className="headerSearchItem">
                        <button onClick={(e)=>handleFormerBookmarks(e)}>
                            Former Bookmarks
                        </button>
            </div>
            <div className="headerSearchItem">
                 <MdAddLocationAlt className="headerIcon locationIcon"/> 
                {/* <div className="headerIcon">📍</div> */}
                <input
                     value={destination}
                     onChange={(e)=> setDestination(e.target.value)}
                     type="text" 
                     placeholder="where to go ?"  
                     className="headerSearchInput"
                     id="destination"
                     />
                     <span className="seperator"></span>
            </div>
            <div className="headerSearchItem">
                <HiCalendar className="headerIcon dateIcon" />
                <div onClick={()=> setOpenDate(!openDate)} className="dateDropDown">{`${format(date[0].startDate,"mm/dd/yy")} to ${format(date[0].endDate,"MM/dd/yy")}`}2023/11/2</div>
                {openDate && <DateRange
                        onChange={item => setDate([item.selection])} 
                        ranges={date}
                        className="date" 
                        minDate={new Date()}
                        moveRangeOnFirstSelection={true}/>}
                        {/* time should ne in an array, so setDate() should contains an array */}
                <span className="seperator"></span>
            </div>
            <div className="headerSearchItem">
                <div id="optionDropDown" onClick={()=> setOpenOptions(!openOptions)}>
                {options.adult} adult &bull; {options.children} children &bull; {options.room} room
                </div>
                {openOptions && <GuestOptionList setOpenOptions={setOpenOptions} handleOptions={handleOptions} options={options}/>}
                <span className="seperator"></span>
            </div>
            <div className="headerSearchItem">
                <button className="headerSearchBtn" onClick={()=> handleSearch()}>
                    <FaNeuter className="headerIcon"/>
                </button>
            </div>
            <div className="headerSearchItem">
                    <button onClick={()=>handleLogin()}>
                        Login
                    </button>
            </div>
        </div>
        <User />
    </div>
  )
}

export default Header;

function GuestOptionList({options, handleOptions,setOpenOptions}){
    //make a ref to know that user clicked somewhere elese in the component
    const optionsRef=useRef();
    useOutsideClick(optionsRef, "optionDropDown",() =>setOpenOptions(false));
    return(
        <div className="guestOptions" ref={optionsRef}>
            <OptionItem handleOptions={handleOptions} type="adult" options={options} minLimit={1} />
            <OptionItem handleOptions={handleOptions} type="children" options={options} minLimit={0}/>
            <OptionItem handleOptions={handleOptions} type="room" options={options} minLimit={1}/>
        </div>
    )
}

function OptionItem({options,type,minLimit,handleOptions}){
    return(<div className="guestOptionItem">
                <span className="optionText">{type}</span>
                <div className="optionCounter">
                        <button onClick={()=> handleOptions(type,"dec")} className="optionCounterBtn" disabled={options[type]<= minLimit}>-</button>
                        <span className="optionCounterNumber">{options[type]}</span>
                        <button onClick={()=> handleOptions(type,"inc")} className="optionCounterBtn">+</button>
                </div>
            </div>
    )
}

function User(){
    const navigate=useNavigate();
    // show user its authentication and informations
    const {user, isAuthenticated, logout}= useAuth();
    const handleLogout=()=>{
        // the logout function is called from useAuth in line 163
        logout();
        navigate("/");
    };
    return <div>
        {isAuthenticated ? <div> <span>{user.name}</span> <button onClick={handleLogout}> 	&#8594; Logout</button> </div> : <NavLink to="/login">Login</NavLink>}
    </div>
}