import React from 'react'
import { Outlet } from 'react-router-dom'
import Map from '../Map/Map'
import { useHotels } from '../context/HotelsProvider'

const  AppLayout = () => {
  const {hotels}= useHotels();
  return (
    <div className="appLayout">
        <div className="sidebar">
            <Outlet /> 
            {/* component will be passed fromm App.jsx base on the <Route /> */}
        </div>
        <Map markerLocations={hotels}/>
    </div>
  )
}

export default  AppLayout