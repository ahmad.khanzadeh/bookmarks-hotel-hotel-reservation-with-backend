import React from 'react'
import Map from '../Map/Map'
import { Outlet } from 'react-router-dom';

const BookmarkLayout = ({markerLocations}) => {
  return (
    <div className="appLayout"> 
        <div className="sidebar">
            bookmarked websites
            <Outlet />
        </div>
        <Map markerLocations={[]} />
    </div>
  );
}

export default BookmarkLayout