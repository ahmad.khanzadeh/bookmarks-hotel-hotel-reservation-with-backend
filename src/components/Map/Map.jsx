import React from 'react'
import { MapContainer, TileLayer , Marker, Popup, useMap, useMapEvent} from 'react-leaflet';
import { useState, useEffect } from 'react';
import { useHotels } from '../context/HotelsProvider'
import { useNavigate, useSearchParams } from 'react-router-dom';
import useGeoLocation from '../../hooks/useGeoLocation';
import useUrlLocation from '../../hooks/useUrlLocations';


const Map = ({markerLocations}) => {
    //what are the hotels which user search for
    const {isLoading, hotels}= useHotels();
    //where the map should be located
    const [mapCenter, setMapCenter]= useState([35.7219,51.3347]);
    //get the location on each hotel by the address bar
    const [searchParams, setSearchParams]= useSearchParams()
    /*
    const lat=searchParams.get("lat");
    const lng=searchParams.get("lng");
    since it was used in other parts too, we turn it to a custom hook
    */
   const [lat,lng]=useUrlLocation();

    //get the user location by the custom geolocation hook
    // const { isLoading: isLoadingPosition , position: geoLocationPosition, getPosition }= useGeoLocation();
    const {
      isLoading: isLoadingPosition,
      position: geoLocationPosition,
      getPosition,
    } = useGeoLocation();
    

      // to save the last searched location by the user in the memory: 
      useEffect(()=>{
           if(lat && lng) setMapCenter([lat,lng]);
        },[lat, lng]);

        // when user press the button and we got his location, change the map center
        useEffect(() => {
          if (geoLocationPosition?.lat && geoLocationPosition?.lng){
            setMapCenter([geoLocationPosition.lat, geoLocationPosition.lng]);}
            
        }, [geoLocationPosition]);

  return (
    <div className="mapContainer">
          <MapContainer 
            className="map" 
            center={mapCenter} 
            zoom={13} 
            scrollWheelZoom={true}
            >
            <button className="getLocation" onClick={getPosition}> {isLoadingPosition? "is Loading...": "Use Your Current Location"} </button>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"
            />
            {/* change the location based on new searched hotels- the function bellow */}
            {/* <ChangeCenter position={[lat || 35.7219,lng || 51.3347]} /> */}
            <ChangeCenter position={mapCenter} />
            <DetectClick />
            {markerLocations.map(item => (
              <Marker key={item.id} position={[item.latitude, item.longitude]}>
                <Popup>
                   {item.host_location}
                 </Popup>
               </Marker>
            ))}
            
          </MapContainer>
        
    </div>
  )
}

export default Map


function ChangeCenter({position}){
  const map=useMap();
  map.setView(position);
  return null;
}

// to see what point did user click in map ( for bookmarks)
function DetectClick(){
  const navigate = useNavigate()
  // this is made by the map provider 
  useMapEvent({
    click: e => navigate(`/bookmark/add?lat=${e.latlng.lat}&lng=${e.latlng.lng}`)
  });
  return null;
}