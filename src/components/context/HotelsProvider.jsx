import React from 'react'
import { createContext, useContext , useState} from 'react'
import useFetch from '../../hooks/useFetch';
import { useSearchParams } from 'react-router-dom';
import  axios  from 'axios';
import {toast} from "react-hot-toast";


const HotelContext=createContext();

const HotelsProvider = ({ children }) => {
    const [searchParams, setSearchParams]=useSearchParams();
    const destination=searchParams.get("destination");
    const room=JSON.parse(searchParams.get("options"))?.room;
    // the last hotel that user look
    const [currentHotel, setCurrentHotel]=useState(null);
    const [isLoadingCurrHotel, setIsLoadingCurrHotel]=useState(false);

    const {isLoading, data: hotels}=useFetch(
        "http://localhost:5000/hotels",
        `q=${destination || ""}&accommodates_gte=${room || 1}`
        );
    
    // what was the last thing that user check among hotels
    async function getHotel(id){
      setIsLoadingCurrHotel(true);
      try{
          const {data}= await axios.get(`http://localhost:5000/hotels/${id}`);
          setCurrentHotel(data);
          setIsLoadingCurrHotel(false);
      } catch(error){
          toast.error(error.message);
          console.log(error.message);
          setIsLoadingCurrHotel(false);
      }
    }

  return (
    <HotelContext.Provider value={{ isLoading, hotels, currentHotel , getHotel, isLoadingCurrHotel}}>{children}</HotelContext.Provider>
  )
}

export default HotelsProvider

export function useHotels(){
    return useContext(HotelContext);
}