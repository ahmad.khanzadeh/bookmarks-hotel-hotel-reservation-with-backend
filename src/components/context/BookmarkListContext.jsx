import React, { useReducer } from 'react'
import { createContext, useContext , useState, useEffect} from 'react'
import useFetch from '../../hooks/useFetch';
import  axios  from 'axios';
import {toast} from "react-hot-toast";


const BookmarkContext=createContext();

const initialState = {
  bookmarks:[],
  isLoading:false,
  currentBookmark: null,
  error:null, 
}

function bookmarkReducer(state, action){
  // should handle 3 status: 1.Pending 2.sucess 3.rejected
    switch(action.type){
      case "loading": return { ...state,isLoading: true,};
      case "bookmarks/loaded": return{ ...state, bookmarks: action.payload, isLoading:false,};
        case "bookmark/loaded": return{...state, isLoading:false, currentBookmark: action.payload}
        case "bookmark/created": return{...state, isLoading:false, bookmarks: [...state.bookmarks, action.payload],currentBookmark: action.payload}
        case "bookmark/deleted": return{...state, isLoading:false, bookmarks:state.bookmarks.filter((item) => item.id !== action.payload), currentBookmark:null}
          case "rejected": return{...state, isLoading:false, error: action.payload,};
            default: throw new Error("Unknown action")
    }
}

const BookmarkListProvider = ({ children }) => {
    /* comment to use the reducer function
    // the last hotel that user look
    const [currentBookmark, setCurrentBookmark]=useState(null);
    // const [isLoadingCurrBookmark, setIsLoadingCurrBookmark]=useState(false); 

    // const {isLoading, data: bookmarks}=useFetch("http://localhost:5000/bookmarks");

    // to call the new added bookmarks
    const [bookmarks, setBookmarks] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    */

    // to use reducer function
    const [{bookmarks,isLoading,currentBookmark},dispatch]=useReducer(bookmarkReducer, initialState);

    // fetch data after the posting new bookmark to backend
    // same as getBookmark, but it doesn't ask for an id:- also in axios.get you should remove the id at the end
    useEffect(()=>{
      async function fetchBookmarkList(){
        // setIsLoading(true);
          dispatch({type: "loading"});
        try{
            const {data}= await axios.get(`http://localhost:5000/bookmarks`);
            // setBookmarks(data);
            dispatch({type:"bookmarks/loaded",payload: data });
        } catch(error){
            toast.error(error.message);
            dispatch({type:"rejected", payload: error.message});
        } finally{
          // setIsLoading(false);
        }
      } 
      // if we dont call it here, none of the bookmarkList in the database will be displayed on the route: "localhost:5173/bookmark"
      fetchBookmarkList()
    },[])

    //  a function which will delete the bookmark item: 
    async function deleteBookmark(id){
      // setIsLoading(true);
      dispatch({type:'loading'});
      try{
          await axios.delete(`http://localhost:5000/bookmarks/${id}`);
          // setBookmarks(prev => prev.filter(item => item.id !== id));
          dispatch({type:"bookmark/deleted",payload: id});
        }catch (error){
          toast.error(error.message);
          dispatch({type:"rejected", payload: error.message});
      } finally{
          // setIsLoading(false);
      }
    }
    
    // what was the last thing that user check among hotels
    async function getBookmark(id){
      // if the last selected item is eqaul to the current one, don't fetch it again.
      if(Number(id)=== currentBookmark?.id) return ;
      // setIsLoading(true);
      dispatch({type:'loading'});
      // setCurrentBookmark(null);
      
      try{
          const {data}= await axios.get(`http://localhost:5000/bookmarks/${id}`);
          // setCurrentBookmark(data);
          dispatch({type: "bookmark/loaded", payload:data});
          // setIsLoading(false);
      } catch(error){
          toast.error(error.message);
          // setIsLoading(false);
          dispatch({type: "rejected", payload:error.message });
      }
    } 


    // a function which will save bookmark data on the db.json
    async function createBookmark(newBookmark){
      // setIsLoading(true);
      dispatch({type: "loading"})
      try{
        const {data}= await axios.post(`http://localhost:5000/bookmarks/`, newBookmark);
        // setCurrentBookmark(data);
        // // fetch new data from the backend
        // setBookmarks((prev) => [...prev, data]);
        dispatch({type:'bookmark/created', payload: data})
      }catch (error){
          toast.error(error.message);
          dispatch({type: "rejected", payload: error.message});
      } finally{
        // setIsLoading(false);
      }
    }



  return (
    <BookmarkContext.Provider value={{ isLoading, bookmarks, currentBookmark ,getBookmark ,createBookmark,deleteBookmark}}>{children}</BookmarkContext.Provider>
  )
}

export default BookmarkListProvider

export function useBookmark(){
    return useContext(BookmarkContext);
}