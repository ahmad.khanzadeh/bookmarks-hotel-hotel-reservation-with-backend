import { createContext, useContext, useReducer } from "react";

const AuthContext= createContext();

const initialState={
    user: null,
    isAuthenticated: false,
}

function authReducerFunction(state,action) {
    switch (action.type) {
        case "login":
            return {
                user: action.payload,
                isAuthenticated: true,
            };
        case "logout":
            return {
                user:null,
                isAuthenticated:false,
            };
        default:
            throw new Error("Unknown action!");
    }
}

//the user which can log in : 
const FAKE_USER={
    name:'samyar',
    email:'samyar@gmail.com',
    password:"1234",
}

export default function AuthContextProvider({children}){
   const [{ user, isAuthenticated },dispatch]= useReducer(authReducerFunction, initialState);
    
   function login(email, password){
        if(email ===FAKE_USER.email && password===FAKE_USER.password){
            dispatch({type:"login", payload: FAKE_USER});
        }
   }

   function logout(){
        dispatch({type: "logout"});
   }
   
   return <AuthContext.Provider value={{user, isAuthenticated, login, logout}}>{children}</AuthContext.Provider>
}

// to export Authcontext in other functions:
export function useAuth(){
    return useContext(AuthContext);
}