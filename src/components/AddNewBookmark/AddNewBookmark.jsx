import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import useUrlLocation from '../../hooks/useUrlLocations';
import { useState } from 'react';
import axios from "axios";
import  Loader  from "../../components/Loader/Loader"
import ReactCountryFlag from 'react-country-flag';
import { useBookmark } from '../context/BookmarkListContext';

// the function which turns countrycode to its flag, from stackOverflow
function getFlagEmoji(countryCode) {
    const codePoints = countryCode
      .toUpperCase()
      .split('')
      .map(char =>  127397 + char.charCodeAt());
    return String.fromCodePoint(...codePoints);
  }

// the url which transfer lng and lat to the city name
const BASE_GEOCODING_URL="https://api.bigdatacloud.net/data/reverse-geocode-client";

const AddNewBookmark = () => {
    const [lat,lng]=useUrlLocation();
    //for back button
    const navigate=useNavigate();
    const [cityName, setCityName]=useState("");
    const [country, setCountry]=useState("");
    const [countryCode, setCountryCode]=useState("");
    const [isLoadingGeoCoding, setIsLoadingGeoCoding]=useState(false);
    const [geoCodingError,setGeoCodingError]= useState(null);
    const { createBookmark }=useBookmark()

    useEffect(()=>{
        // if there were no data in addressbar, stop it !
        if(!lat || !lng) return;

        async function fetchLocationData(){
            setIsLoadingGeoCoding(true);
            setGeoCodingError(null);
            try{
                const { data }= await axios.get(`${BASE_GEOCODING_URL}?latitude=${lat}&longitude=${lng}`);
                // if clicked location was not in a country ( like it was in the middle of sea)
                if(!data.countryCode) throw new Error("This location is not in a city!, please click somewhere else.")

                setCityName(data.city || data.locality || "");
                setCountry(data.countryName);
                 setCountryCode(data.countryCode);
                // just in server..not working in windows
                // setCountryCode(getFlagEmoji(data.countryCode));
            }catch(error){
                    setGeoCodingError(error.message);
            } finally {
                setIsLoadingGeoCoding(false);
            }
        }
        // important to call it
        fetchLocationData();
    },[lat,lng])

    const handleSubmit= async (e)=>{
        e.preventDefault();
        if(!cityName || !country) return;

        const newBookmark={ cityName, country, countryCode, latitude: lat, longitude: lng, host_location: cityName + " " + country};
         await createBookmark(newBookmark);
        //  now navigate the user to the bookmark page
        navigate("/bookmark")
    }

    if(isLoadingGeoCoding) return <Loader />
    // if the location was not in a country( error !data.country ) => catch(error) is fired and setGeoCodingError( ) will be called;
    if(geoCodingError) return <p>{geoCodingError}</p>
  return (
    <div>
        <h2>Bookmark New Location</h2>
        <form className="form" onSubmit={handleSubmit}>
            <div className="formControl">
                <label htmlFor="cityName">City Name</label>
                <input type="text" name="cityName" id="cityName" placeholder="city"/>
            </div>
            <div className="formControl">
                <label htmlFor="country">Country</label>
                <input type="text" name="country" id="country"  placeholder="country"/>
                <ReactCountryFlag className="flag" svg countryCode={countryCode}/>
                <span className="flag">{countryCode}</span>
            </div>
            <div className="buttons">
                <button className="btn btn--back" onClick={(e)=> {e.preventDefault(); navigate(-1)}}> &larr; back</button>
                <button className="btn btn--primary">add to bookmark</button>
            </div>
        </form>
    </div>
  )
}

export default AddNewBookmark